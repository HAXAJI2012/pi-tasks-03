#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
int main()
{
	srand(time(0));
	int i, num, length = 1, last = 1, amount = 0;
	int arr[256];
	printf("Enter amount of numbers in a massive (from 1 to 256): ");
	if (scanf("%d", &amount) == 0 || !(amount >= 1 && amount <= 256))
	{
		printf("INPUT ERROR!\n");
		return 1;
	}
	for (i = 0; i < amount; i++)
	{
		arr[i] = rand() % (10 - 0 + 1) + 0;
		if (arr[i] == arr[i - 1])
			length++;
		else if (last == length)
			length = 1;
		else if (last < length)
		{
			last = length;
			num = arr[i - 1];
			length = 1;
		}
	}
	if (last < length)
	{
		last = length;
		num = arr[i - 1];
	}
	else if (last == 1)
		num = arr[0];
	printf("Now I'm writing your array...\n");
	for (i = 0; i < amount; i++)
		printf("%d ", arr[i]);
	printf("\nNow get your result!\n");
	for (i = 0; i < last; i++)
		printf("%d\t", num);
	printf("\nLength: %d\n", last);
	return 0;
}